# SPDX-FileCopyrightText: 2022 Endo Renberg
# SPDX-License-Identifier: AGPL-3.0-or-later

## Serve CoAP over TCP (RFC 8323).
## https://datatracker.ietf.org/doc/html/rfc8323

import std/[algorithm, asyncfutures, bitops, net, random, sequtils]
from std/options import some
import taps # sockets? nein danke
import ./common

export common

const codeCsm = code(7, 1)

type
  Message* = object
    ## Type for sending and receiving CoAP messages.
    payload*: seq[byte]
    options*: seq[Option]
    token*: Token
    code*: Code

proc errorDiagnostic*(msg: Message): string =
  ## Return the diagnostic string for an error response, if there is
  ## one present. `ValueError` is raised if the message is not an error.
  if msg.code.class notin {clientError, serverError}:
    raise newException(ValueError, "Message is not an error response")
  result = cast[string](msg.payload)

type
  Session* = ref object of RootObj
    ## Base type of session dispatchers.
    conn: Connection

proc close*(state: Session) =
  close(state.conn)

proc send(conn: Connection; msg: sink Message) =
  ## Send `msg` with `conn`.
  ## Options at `msg.options` are sorted before transmission.
  var tkl: uint8
  if msg.token != 0:
    tkl = uint8(max(1, (64 - countLeadingZeroBits(msg.token)) shr 3))
  var msgLen = 0
  block:
    var prevNum = 0
    for opt in msg.options:
      msgLen = msgLen + 1 + opt.data.len
      var delta = opt.num - prevNum
      if delta < 13: discard
      elif delta < 269:
        inc(msgLen, 1)
      else:
        inc(msgLen, 2)
      if opt.data.len < 13: discard
      elif opt.data.len < 269:
        inc(msgLen, 1)
      else:
        inc(msgLen, 2)
      prevNum = opt.num
  if msg.payload.len > 0:
    inc(msgLen, 1 + msg.payload.len)
  var header = newSeqOfCap[byte](11 + msgLen - msg.payload.len)
  if msgLen < 13:
    header.add(tkl or (uint8 msgLen shl 4))
  elif msgLen < 269:
    header.add(tkl or (13'u8 shl 4))
    msgLen.dec 13
    header.add(uint8 msgLen)
  elif msgLen < 65805:
    header.add(tkl or (14'u8 shl 4))
    msgLen.dec(269)
    for i in countdown(1, 0): header.add(uint8 (msgLen shr (i shl 3)))
  else:
    header.add tkl or (15'u8 shl 4)
    msgLen.dec 65805
    for i in countdown(3, 0): header.add(uint8 (msgLen shr (i shl 3)))
  header.add(uint8 msg.code)
  if tkl > 0:
    for i in countdown(tkl-1, 0): header.add(uint8 msg.token shr (i shl 3))
  sort(msg.options) do (x, y: Option) -> int: cmp(x.num, y.num)
  block:
    var prevNum = 0
    for opt in msg.options:
      assert prevNum <= opt.num
      let optOff = header.len
      var delta = opt.num - prevNum
      if delta < 13:
        header.add(uint8 delta shl 4)
      elif delta < 269:
        header.add(13'u8 shl 4)
        dec(delta, 13)
        header.add(uint8 delta)
      else:
        header.add(14'u8 shl 4)
        dec(delta, 269)
        header.add(uint8 delta shr 8)
        header.add(uint8 delta and 0xff)
      var optLen = opt.data.len
      if optLen < 13:
        header[optOff] = header[optOff] or optLen.uint8
      elif optLen < 269:
        header[optOff] = header[optOff] or 13'u8
        dec(optLen, 13)
        header.add(uint8 optLen)
      else:
        header[optOff] = header[optOff] or 14'u8
        dec(optLen, 269)
        header.add(uint8 optLen shr 8)
        header.add(uint8 optLen and 0xff)
      if opt.data.len > 0:
        header.add(opt.data)
      prevNum = opt.num
  send(conn, header, endOfMessage = (msg.payload.len == 0))
  if msg.payload.len > 0:
    send(conn, [0xff'u8], endOfMessage = false)
    send(conn, msg.payload, endOfMessage = true)

proc send*(state: Session; msg: sink Message) =
  ## Send `msg` in `Sesssion` `state`.
  ## Options at `msg.options` are sorted before transmission.
  send(state.conn, msg)

method onMessage*(state: Session; msg: Message) {.base, gcsafe.} =
  ## Method for dispatching a `Message`.
  ## Application dispatchers must inherit `Session` and overload this method.
  raiseAssert("onMessage not implemented for this Session type")

method onError*(state: Session; error: ref Exception) {.base, gcsafe.} = raise error
  ## Method for dispatching exceptions.
  ## The base behavior is to re-raise the exception.
  ## Override this method if you must clean up state or discard errors.

proc receiveMessage(conn: Connection; fut: FutureVar[Message])  =
  conn.onReceived do (buf: seq[byte]; ctx: MessageContext):
    assert(buf.len == 1)
    var
      tkl = int buf[0] and 0b1111
      msgLen = int buf[0] shr 4
      extLen = case msgLen
        of 15: 4
        of 14: 2
        of 13: 1
        else: 0
    let recvLen = extLen + 1 + tkl
    conn.onReceived do (buf: seq[byte]; ctx: MessageContext):
      if buf.len == recvLen: # TODO: short read, connection is closing?
        if extLen > 0:
          msgLen = 0
          for i in 0..<extLen:
            msgLen = (msgLen shl 8) or buf[i].int
          case extLen
          of 4: inc(msgLen, 65805)
          of 2: inc(msgLen, 269)
          of 1: inc(msgLen, 13)
          else: discard
        var off = extLen
        fut.mget.code = Code buf[off]
        off.inc
        fut.mget.token = 0
        for i in 0..<tkl: fut.mget.token = (fut.mget.token shl 8) or buf[off+i].Token
        off.inc tkl
        if msgLen == 0:
          fut.complete()
        else:
          conn.onReceived do (buf: seq[byte]; ctx: MessageContext):
            if buf.len == msgLen: # TODO: short read, connection is closing?
              var off, optNum: int
              while off < buf.len:
                if buf[off] == 0xff:
                  inc off
                  break
                var
                  delta = int buf[off] shr 4
                  optLen = int buf[off] and 0b1111
                inc off
                case delta
                of 15:
                  raise newException(ValueError, "invalid CoAP option delta")
                of 14:
                  optNum = optNum + (buf[off].int shl 8) + buf[off+1].int + 269
                  inc(off, 2)
                of 13:
                  optNum = optNum + buf[off].int + 13
                  inc(off, 1)
                else:
                  inc(optNum, delta)
                case optLen
                of 15:
                  raise newException(ValueError, "invalid CoAP option length")
                of 14:
                  optLen = (buf[off].int shl 8) + buf[off+1].int + 269
                  inc(off, 2)
                of 13:
                  optLen = buf[off].int + 13
                  inc(off, 1)
                else: discard
                var option =
                  if optLen > 0:
                    Option(num: optNum, data: buf[off..pred(off+optLen)])
                  else:
                    Option(num: optNum)
                fut.mget.options.add option
                inc(off, optLen)
              if off < buf.high: fut.mget.payload = buf[off..buf.high]
              fut.complete()
        conn.receive(minIncompleteLength = msgLen, maxLength = msgLen)
    conn.receive(minIncompleteLength = recvLen, maxLength = recvLen)
  conn.receive(maxLength = 1)

proc receiveMessage(state: Session; loop: bool) {.gcsafe.} =
  var fut = newFutureVar[Message]("receiveMessage")
  cast[Future[Message]](fut).addCallback do (f: Future[Message]):
    if f.failed: raise f.error
    else:
      var msg = fut.read
      try: state.onMessage(msg)
      except CatchableError as e:
        # TODO: maybe the connection is closed?
        var resp = Message(
          code: code(5,0),
          token: msg.token,
          payload: cast[seq[byte]](e.msg))
        state.send(resp)
        raise e
      if loop:
        fut.clean()
        callSoon:
          receiveMessage(state, loop)
  receiveMessage(state.conn, fut)

type InitialSession = ref object of Session
  ## Type for exchanging Capabilities and Settings Messages  (CSMs) with clients.
  app: Session
  maxMessageSize: int
  blockWiseTransfer: bool

method onMessage(state: InitialSession; msg: Message) =
  ## Check that `msg` is a CSM then transfer to the application dispatcher.
  var fail: bool
  if msg.code != codeCsm: fail = true
  for opt in msg.options:
    if not fail:
      case opt.num
      of 2: # Max-Message-Size
        # TODO: convenience getters
        if not fromOption(state.maxMessageSize, opt):
          fail = true
      of 4: # Block-Wise-Transfer
        if opt.data.len == 0: state.blockWiseTransfer = true
        else: fail = true
      else: discard
  if msg.payload.len != 0: fail = true
  if fail: close(state.conn)
  else:
    var msg = msg
    send(state, msg) # bounce the CSM back
    state.app.conn = state.conn
    receiveMessage(move state.app, loop = true)

type
  Server* = ref object of RootObj
    ## Object representing a CoAP endpoint that services TCP connections.
    listener: Listener

method createSession*(server: Server): Session {.base, gcsafe.} =
  ## Method for creating a new `Session` instances for incoming sessions.
  # TODO: session metadata
  raiseAssert("createSession not implemented for this Session type")

proc stop*(serv: Server) =
  ## Stop `serv` from accepting new session.
  stop(serv.listener)

proc serve*(server: Server; ipAddr = parseIpAddress("::"); port = Port(5683)) =
  ## Dispatch CoAP message on `port` using a `Session` type `T`.
  ## Dispatching is asynchronous, the asyndispatcher must be driven
  ## independently.
  runnableExamples:
    import std/asyncdispatch, coap/tcp

    type EchoSession = ref object of Session

    method onMessage(state: EchoSession; msg: Message) =
      var resp = Message(token: msg.token)
      if msg.code == GET:
        resp.code = code(2,5)
        resp.payload = cast[seq[byte]]("Hello world!")
      else:
        resp.code = code(5,1)
      send(state, resp)

    let server = serve(EchoSession)

    poll()

  var lp = newLocalEndpoint()
  lp.with ipAddr
  lp.with port

  var tp = newTransportProperties()
  tp.require("reliability")
  tp.require("preserve-order")

  var preconn = newPreconnection(
    local = @[lp], transport = tp.some)
  server.listener = listen(preconn)
  server.listener.onListenError do (err: ref Exception): raise err
  server.listener.onConnectionReceived do (conn: Connection):
    var session = server.createSession()
    proc msgErrCb (ctx: MessageContext; reason: ref Exception) =
      close(session.conn)
      session.onError(reason)
    conn.onReceiveError(msgErrCb)
    conn.onSendError(msgErrCb)
    receiveMessage(InitialSession(conn: conn, app: session), loop = false)

type
  Client* = ref object
    conn: Connection
    pending: seq[tuple[future: Future[Message], token: Token]]
      # an indexable sequence of pending response futures,
      # length is aways one or a power of two
    futVar: FutureVar[Message]
    rng: Rand

proc setup(clientFut: Future[Client]; client: Client) =
  client.pending.setLen(1)
  client.conn.onInitiateError do (err: ref Exception): fail(clientFut, err)
  client.conn.onReady do ():
    send(client.conn, Message(code: codeCsm))
    client.conn.onSent do (ctx: MessageContext):
      var respFut = newFutureVar[Message]("setup")
      receiveMessage(client.conn, respFut)
      cast[Future[Message]](respFut).addCallback do (respFut: Future[Message]):
        if respFut.failed:
          clientFut.fail(respFut.readError)
        else:
          var resp = respFut.read
          if resp.code == codeCsm:
            clientFut.complete client
          elif resp.code.class == serverError:
            clientFut.fail newException(CatchableError, $resp.code & ": " & resp.errorDiagnostic)
          else:
            clientFut.fail newException(CatchableError, "invalid server response code " & $resp.code)
  client.futVar = newFutureVar[Message]("coap.client")
  proc receiveCallback {.gcsafe.} =
    if cast[Future[Message]](client.futVar).failed:
      while client.pending.len > 0:
        fail(client.pending.pop().future, cast[Future[Message]](client.futVar).readError)
    else:
      let
        resp = client.futVar.read
        i = resp.token.int and client.pending.high
      if client.pending[i].token == resp.token:
        var fut = move client.pending[i].future
        reset(client.pending[i])
        complete(fut, resp)
    clean(client.futVar)
    addCallback(cast[Future[Message]](client.futVar), receiveCallback)
  addCallback(cast[Future[Message]](client.futVar), receiveCallback)

type CoapUri* = common.Uri

proc connect*(uri: CoapUri): Future[Client] =
  doAssert uri.kind == coapTcpUrl, $uri.kind & " not implemented"
  var tcpProp = newTransportProperties()
  tcpProp.require "reliability"
  tcpProp.require "preserve-order"
  var
    preConn = newPreConnection(remote = @[uri.endpoint], transport = some tcpProp)
    client = Client(conn: preConn.initiate(), rng: initRand())
  result = newFuture[Client]"connect"
  setup(result, client)

proc connect*(uri: string): Future[Client] =
  var x: Uri
  if not x.fromString uri:
    raise newException(ValueError, "invalid CoAP URI")
  connect(x)

proc close*(client: Client) =
  close(client.conn)

proc request*(client: Client; req: var Message): Future[Message] =
  assert req.code.class == 0.Class
  var tokenAttempts: int
  while true:
    if tokenAttempts >= client.pending.len:
      setLen(client.pending, client.pending.len shl 1)
    req.token = Token rand(client.rng, max(0xff, client.pending.high))
    let i = req.token.int and client.pending.high
    if client.pending[i].future.isNil:
      break
    inc tokenAttempts
      # create a random token that can uniquely index
      # a future in the client.pending sequence
  let token = req.token
  client.conn.send req
  var fut = newFuture[Message]"request"
  let i = token.int and client.pending.high
  assert(client.pending[i].future.isNil, $i)
  client.pending[i] = (fut, token)
  client.conn.onSent do (ctx: MessageContext):
    receiveMessage(client.conn, client.futVar)
  fut

proc GET*(client: Client; path: openarray[string]): Future[Message] =
  var req = Message(code: codeGET)
  req.options = path.map do (elem: string) -> Option:
    elem.toOption(optUriPath)
  request(client, req)

proc DELETE*(client: Client; path: openarray[string]): Future[Message] =
  var req = Message(code: codeDELETE)
  req.options = path.map do (elem: string) -> Option:
    elem.toOption(optUriPath)
  request(client, req)
